package hu.iqjb.rest.simple.repository;

import hu.iqjb.rest.simple.entity.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserRepository {

    private List<User> database;

    public UserRepository() {
        database = new ArrayList<>();
        User user = new User();
        user.setId(1);
        user.setEmail("email@email.com");
        user.setName("name1");
        database.add(user);
    }

    public List<User> findAll() {
        return database;
    }

    public void add(User user){
        database.add(user);
    }


}
