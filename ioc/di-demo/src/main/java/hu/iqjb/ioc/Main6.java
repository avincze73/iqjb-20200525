package hu.iqjb.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main6 {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");


		
		EmployeeWithCollections employee3 = (EmployeeWithCollections) context.getBean("employee3");
		System.out.println(employee3);

		
		((ConfigurableApplicationContext)context).close();
		
		
	}
}
