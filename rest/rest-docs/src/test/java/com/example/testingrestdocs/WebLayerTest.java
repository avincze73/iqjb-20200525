package com.example.testingrestdocs;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(HomeController.class)
@AutoConfigureRestDocs(outputDir = "target/snippets")
public class WebLayerTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void shouldReturnDefaultMessage() throws Exception {
		this.mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("Hello, World2")))
				.andDo(document("home"));
	}


	@Test
	public void shouldReturnDepartmentById() throws Exception {
		this.mockMvc.perform(RestDocumentationRequestBuilders.get("/department/{id}", 1))
				.andDo(print()).andExpect(status().isOk())


				.andDo(document("department/get-department-by-id",
						pathParameters(parameterWithName("id").description("Department Id")),
						responseFields(
								fieldWithPath("name").description("Department name"),
								fieldWithPath("id").description("Department Id"))
				));
	}

	@Test
	public void shouldReturnDepartmentByName() throws Exception {
		this.mockMvc.perform(RestDocumentationRequestBuilders.get("/department1/{name}", "name1"))

				.andDo(print()).andExpect(status().isOk())
				.andDo(document("department/get-department-by-name",
						pathParameters(parameterWithName("name").description("Department Name")),
						responseFields(
								fieldWithPath("name").description("Department name"),
								fieldWithPath("id").description("Department Id"))
				));
	}
}
