package hu.iqjb.web.demo.controller;

import hu.iqjb.web.demo.properties.IqjbProperties;
import hu.iqjb.web.demo.viewmodel.EmployeeViewModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.support.SessionStatus;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@Slf4j
public class EmployeeController {


    @Autowired
    private EmployeeViewModel employeeViewModel;


    @ModelAttribute("titleList")
    public List<String> getTitleList(){
        List<String> titleList = new ArrayList<>();
        titleList.add("Manager");
        titleList.add("CEO");
        titleList.add("Developer");
        return titleList;
    }

    @GetMapping({"/employee"})
    public String getEmployee(Model model) {
        model.addAttribute("entity", new Employee());
        return "/employee";
    }

    @GetMapping({"/employeelist"})
    public String getEmployees(Model model, Session session) {
        log.info("employeeList: " + employeeViewModel.getEmployeeList().size());
        model.addAttribute("entityList", employeeViewModel.getEmployeeList());
        return "/employees";
    }

    @PostMapping("/employee")
    public String entitySave(@ModelAttribute("entity") @Valid Employee employee, BindingResult errors, SessionStatus status, Model model){
        employee.setId(employeeViewModel.getEmployeeList().size() + 1);
        employeeViewModel.getEmployeeList().add(employee);
        log.info("employeeList: " + employeeViewModel.getEmployeeList().size());
        if (errors.hasErrors()){
            return "/employee";
        }
        status.setComplete();
        return "redirect:/employeelist";
    }

}
