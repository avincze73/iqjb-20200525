package hu.iqjb.web.demo.controller;

import hu.iqjb.web.demo.properties.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Slf4j
public class SimpleController {

    @Autowired
    private IqjbProperties iqjbProperties;

    @RequestMapping({"/simple"})
    public String main(Model model) {
        model.addAttribute("greeting", "Hello from controller");
        model.addAttribute("host", iqjbProperties.getHostname());
        return "/simple";
    }
}
