package hu.iqjb.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main9 {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");


		EmployeeWithInheritance employee7 = (EmployeeWithInheritance) context.getBean("employee7");
		System.out.println(employee7);
		

		
		((ConfigurableApplicationContext)context).close();
		
		
	}
}
