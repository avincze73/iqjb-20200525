package com.example.testingrestdocs;

import java.util.Collections;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

	@GetMapping("/")
	public Map<String, Object> greeting() {
		return Collections.singletonMap("message", "Hello, World2");
	}


	@GetMapping("/department/{id}")
	public Department getById(@PathVariable(name = "id") Integer id) {
		return new Department(id, "name" + id);
	}
	@GetMapping("/department1/{name}")
	public Department getByName(@PathVariable(name = "name") String name) {
		return new Department(1, name);
	}

}
