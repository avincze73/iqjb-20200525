package hu.iqjb.ex5;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class DepartmentService implements Serializable {

    @Autowired
    private Database database;

    @Autowired
    private SessionInfo sessionInfo;


    @LogExecutionTime
    public void add(Department department){
        sessionInfo.setNumber(sessionInfo.getNumber()+1);
        log.info(sessionInfo.getNumber()+" service");
        database.getDepartmentList().add(department);
    }

    @LogExecutionTime
    public List<Department> getAll(){
        sessionInfo.setNumber(sessionInfo.getNumber()+1);
        log.info(sessionInfo.getNumber()+" service");
        return database.getDepartmentList();
    }

    @LogExecutionTime
    public Department getByName(String name){
        return database.getDepartmentList().stream().filter(department -> name.equals(department.getName())).findFirst().get();
    }

    @LogExecutionTime
    public Department getById(Integer id){
        return database.getDepartmentList().stream().filter(department -> id.equals(department.getId())).findFirst().get();
    }

}
