package hu.iqjb.firstspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "hu.iqjb.firstspringboot", exclude = {ErrorMvcAutoConfiguration.class})
@EnableJpaRepositories("hu.iqjb.firstspringboot.repository")
@EntityScan("hu.iqjb.firstspringboot.entity")
@ServletComponentScan
public class FirstSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(FirstSpringBootApplication.class, args);
    }

}
