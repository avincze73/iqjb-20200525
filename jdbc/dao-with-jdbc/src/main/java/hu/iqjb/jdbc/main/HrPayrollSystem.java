package hu.iqjb.jdbc.main;

import hu.iqjb.jdbc.dao.EmployeeDao;
import hu.iqjb.jdbc.dao.EmployeeDaoImpl;
import hu.iqjb.jdbc.model.Employee;

public class HrPayrollSystem {

    public static void main(String[] args) {
        EmployeeDao employeeDao = new EmployeeDaoImpl();
        // create employee table
        employeeDao.createEmployee();
        // insert into employee table
        employeeDao.insertEmployee(new Employee(1, "Attila"));
        // get employee based on id
        Employee employee = employeeDao.getEmployeeById(1);
        System.out.println("Employee name: " + employee.getName());
    }
}
