package hu.iqjb.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main4 {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

		Employee2 employee2 = (Employee2) context.getBean("employee2");
		System.out.println(employee2);

		((ConfigurableApplicationContext)context).close();
		
		
	}
}
