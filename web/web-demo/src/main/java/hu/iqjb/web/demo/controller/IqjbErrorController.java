package hu.iqjb.web.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IqjbErrorController {

    @RequestMapping("/404")
    public String error404(Model model) {
        return "/404";
    }

    @RequestMapping("/500")
    public String error500(Model model) {
        return "/500";
    }

//    @RequestMapping("/error")
//    public String general(Model model) {
//        return "/error";
//    }
}
