package hu.iqjb.ex5;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@SessionScope
@Component
public class Database implements Serializable {

    private List<Department> departmentList;
    private List<Employee> employeeList;

    public Database() {
        departmentList = new ArrayList<>();
        employeeList = new ArrayList<>();
    }
}
