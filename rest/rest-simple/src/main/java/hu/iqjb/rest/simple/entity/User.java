package hu.iqjb.rest.simple.entity;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {

    private long id;

    @NotBlank(message = "Name is mandatory")
    private String name;

    @NotEmpty(message = "{email.notempty}")
    private String email;

    @NotEmpty(message = "{notempty.user.title}")
    private String title;


}
