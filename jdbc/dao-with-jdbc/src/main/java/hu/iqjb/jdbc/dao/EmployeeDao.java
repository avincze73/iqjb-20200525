package hu.iqjb.jdbc.dao;

import hu.iqjb.jdbc.model.Employee;

public interface EmployeeDao {
    Employee getEmployeeById(int id);

    void createEmployee();

    void insertEmployee(Employee employee);
}
