package hu.iqjb.ex5;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@Slf4j
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private DepartmentService departmentService;

    @ModelAttribute("titleList")
    public List<String> getTitleList(){
        List<String> titleList = new ArrayList<>();
        titleList.add("Manager");
        titleList.add("CEO");
        titleList.add("Developer");
        return titleList;
    }


    @ModelAttribute("departmentList")
    public List<Department> getDepartmentList(){
        List<Department> departmentList = new ArrayList<>();
        departmentService.getAll().forEach(department -> departmentList.add(department));
        return departmentList;
    }


    @GetMapping("/employee")
    public String employeeGet(Model model){
        model.addAttribute("entity", new Employee());
        return "employee";
    }

    @GetMapping("/employees")
    public String employeesGet(Model model){
        model.addAttribute("entityList", employeeService.getAll());
        return "employees";
    }

    @PostMapping("/employee")
    public String entitySave(@ModelAttribute @Valid Employee entity,
                             @RequestParam("name") String name,
                             BindingResult errors, SessionStatus status){
        if (errors.hasErrors()){
            return "/employee";
        }
        entity.setDepartment(departmentService.getById(entity.getDepartment().getId()));
        log.info(employeeService.getAll().size() + " ");
        entity.setId(employeeService.getAll().size() + 1);
        employeeService.getAll().add(entity);
        status.setComplete();
        return "redirect:/employees";
    }

}
