package hu.iqjb.ex4;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class EmployeeController {

    @GetMapping("/employee")
    @ResponseBody
    public Employee employee(){
        Employee employee = new Employee();
        employee.setId(1);
        employee.setTitle("title");
        return employee;
    }
}
