package hu.iqjb.ex5;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
public class Employee {
    private Integer id;
    @NotNull
    private String name;
    @NotNull
    private String loginName;
    @NotNull
    private String title;
    @NotNull
    private String password;
    @NotNull
    private Integer salary;
    @NotNull
    private Department department;

    public Employee() {
        department = new Department();
    }
}
