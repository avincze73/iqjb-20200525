package hu.iqjb;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        ApplicationContext context = new AnnotationConfigApplicationContext(InfrastructureConfig.class);
        EmployeeService employeeService = context.getBean(EmployeeService.class);
        Employee employee = new Employee();
        employee.setId(1);
        employee.setTitle("title1");
        employeeService.add(employee);
        employeeService.getAll().forEach(System.out::println);

    }

}
