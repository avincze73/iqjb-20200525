package hu.iqjb.main.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.event.annotation.BeforeTestClass;
import org.springframework.test.web.client.MockRestServiceServer;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RestClientTest(MainService.class)
class MainServiceTests {


	@Autowired
	private MainService mainService;

	@Autowired
	private MockRestServiceServer server;

	@Autowired
	private ObjectMapper objectMapper;

	@BeforeEach
	public void setUp() throws Exception {

		this.server.expect(requestTo("http://localhost:8081/save"))
				.andRespond(withSuccess("detailsString", MediaType.TEXT_PLAIN));

		this.server.expect(requestTo("http://localhost:8082/save"))
				.andRespond(withSuccess("detailsString", MediaType.TEXT_PLAIN));
	}

	@Test
	void contextLoads() {

		this.mainService.start();
	}

}
