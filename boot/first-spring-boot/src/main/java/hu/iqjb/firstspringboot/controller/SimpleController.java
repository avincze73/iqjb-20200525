package hu.iqjb.firstspringboot.controller;

import hu.iqjb.firstspringboot.repository.BookIdMismatchException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SimpleController {

    @Value("${spring.application.name}")
    String appName;

    @GetMapping("/")
    public String homePage(Model model) {
        model.addAttribute("appName", appName);
        return "home";
    }

    @GetMapping("/a")
    public String errorPage(Model model) {
        model.addAttribute("appName", appName);
        throw new BookIdMismatchException();

    }
}
