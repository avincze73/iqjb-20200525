package hu.iqjb;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        EmployeeService employeeService = context.getBean(EmployeeService.class);
        Employee employee = new Employee();
        employee.setId(6);
        employee.setTitle("title6");
        employeeService.add(employee);
        employeeService.getAll().forEach(System.out::println);
    }
}
