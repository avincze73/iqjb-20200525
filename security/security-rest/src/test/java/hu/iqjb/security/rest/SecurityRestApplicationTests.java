package hu.iqjb.security.rest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest({RestController1.class, SecureService.class})
class SecurityRestApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private SecureService secureService;



	@Test
	void shoudGetAccess() throws Exception {
		mockMvc.perform(get("/api1/greeting"))
				.andDo(print())
				.andExpect(status().isOk());
	}

	@Test
	void shoudGetMethodNotAllowed() throws Exception {
		mockMvc.perform(post("/api1/greeting"))
				.andDo(print())
				.andExpect(status().isMethodNotAllowed());
	}

	@Test
	void shoudGetUnauthorizedAccess() throws Exception {
		mockMvc.perform(get("/api1/greeting2"))
				.andDo(print())
				.andExpect(status().isUnauthorized());
	}

	@Test
	//@WithMockUser("admin")
	@WithMockAdminRole
	void shoudGetAccessToProtectedMethod() throws Exception {
		mockMvc.perform(get("/api1/greeting2"))
				.andDo(print())
				.andExpect(status().isOk());
	}

	@Test
	void shoudGetGetGatewayStatusCode() throws Exception {
		mockMvc.perform(get("/api1/greeting3/{id}", "11"))
				.andDo(print())
				.andExpect(status().isBadGateway());
	}

	@Test
	void shoudGetFailedDependencyStatusCode() throws Exception {
		mockMvc.perform(get("/api1/greeting3/{id}", "22"))
				.andDo(print())
				.andExpect(status().isFailedDependency());
	}

	@Test
	void shoudGetBadRequestStatusCode() throws Exception {
		mockMvc.perform(get("/api1/language"))
				.andDo(print())
				.andExpect(status().isBadRequest());
	}

	@Test
	void shoudGetHttpHeaders() throws Exception {
		mockMvc.perform(get("/api1/listheaders"))
				.andDo(print())
				.andExpect(status().isOk())
		.andExpect(content().string(containsString("headers")));
	}

	@Test
	void shoudGetLanguage() throws Exception {
		mockMvc.perform(get("/api1/language2"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().string(containsString("hu")));
	}

//	@Test
//	void shoudGetNotModified() throws Exception {
//		mockMvc.perform(get("/api1/etag").header("If-None-Match", 1234))
//				.andDo(print())
//				.andExpect(status().isNotModified());
//	}

}
