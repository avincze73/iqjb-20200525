package hu.iqjb.security.rest;

import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.codec.binary.Base64;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.web.client.RestTemplate;

import java.net.URL;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
public class SystemTest {

    @LocalServerPort
    private int port;

    private URL base;

    @Autowired
    private TestRestTemplate template;

    @Test
    void shouldReturnUnsecure() throws Exception {
        RestTemplate restTemplate = new RestTemplateBuilder().build();
        this.base = new URL("http://localhost:" + port + "/api1/greeting");
        ResponseEntity<String> response = template.getForEntity(base.toString(),
                String.class);
        assertThat(response.getBody(), containsString("unsecure"));
    }

    @Test
    void shouldReturnUnauthorized() throws Exception {
        this.base = new URL("http://localhost:" + port + "/api2/greeting");
        ResponseEntity<String> response = template.getForEntity(base.toString(),
                String.class);
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    void shouldReturnOK() throws Exception {
        RestTemplate restTemplate = new RestTemplateBuilder().build();
        String plainCreds = "admin:admin";
        byte[] plainCredsBytes = plainCreds.getBytes();
        byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
        String base64Creds = new String(base64CredsBytes);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Creds);
        HttpEntity<String> request = new HttpEntity<String>(headers);
        ResponseEntity<String> response = restTemplate.exchange(
                new URL("http://localhost:" + port + "/api2/greeting").toString(),
                HttpMethod.GET, request, String.class);
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void shouldReturnOKAgain() throws Exception {
        RestTemplate restTemplate = new RestTemplateBuilder()
                .basicAuthentication("admin", "admin").build();
        this.base = new URL("http://localhost:" + port + "/api2/greeting");
        ResponseEntity<String> response = restTemplate.getForEntity(base.toString(),
                String.class);
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void shouldReturnOKWithTestRestTemplate() throws Exception {
        this.base = new URL("http://localhost:" + port + "/api2/greeting");
        ResponseEntity<String> response = template.withBasicAuth("admin", "admin")
                .getForEntity(base.toString(), String.class);
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }


    @Test
    void shouldReturnOKAgain2() throws Exception {
        RestTemplate restTemplate = new RestTemplateBuilder().basicAuthentication("admin", "admin").build();
        this.base = new URL("http://localhost:" + port + "/api2/greeting");
        ResponseEntity<String> response = restTemplate.getForEntity(base.toString(),
                String.class);
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }


}
