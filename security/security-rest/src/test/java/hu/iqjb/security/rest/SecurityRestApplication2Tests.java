package hu.iqjb.security.rest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.cli.CliDocumentation;
import org.springframework.restdocs.http.HttpDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;

@WebMvcTest(RestController2.class)
//@AutoConfigureRestDocs(outputDir = "target/snippets")
//@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
class SecurityRestApplication2Tests {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext context;



	@Test
	void shoudGetUnauthorizedAccess() throws Exception {
		mockMvc.perform(get("/api2/greeting"))
				.andDo(print())
				.andExpect(status().isUnauthorized())

		//.andDo(document("/home"))
		;
	}

	//@WithMockUser("admin")
	@WithMockAdminRole
	@Test
	void shouldAccessProtectedResource() throws Exception {
		mockMvc.perform(get("/api2/greeting"))
				.andDo(print())
				.andExpect(status().isOk())
		.andExpect(content().string(containsString("secure")));
	}

}
