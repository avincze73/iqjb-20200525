package hu.iqjb.ex5;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

import java.util.logging.Logger;

@Aspect
@Configuration
public class LogExecutionTimeAspect {

    private static final Logger logger = Logger.getLogger(LogExecutionTimeAspect.class.getName());


    @Around("@annotation(hu.iqjb.ex5.LogExecutionTime)")
    public Object logExecutionTimeOfMethodCall(ProceedingJoinPoint joinPoint) throws Throwable {
        long start = System.currentTimeMillis();

        Object proceed = joinPoint.proceed();

        long executionTime = System.currentTimeMillis() - start;

        logger.info(joinPoint.getSignature() + " executed in " + executionTime + "ms");
        return proceed;
    }

}
