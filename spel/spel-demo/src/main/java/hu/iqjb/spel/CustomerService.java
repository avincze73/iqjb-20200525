package hu.iqjb.spel;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class CustomerService {

    @Value("#{customer.map['emp1']}")
    private String mapElement;
    @Value("#{customer.list[0]}")
    private String listElement;

}
