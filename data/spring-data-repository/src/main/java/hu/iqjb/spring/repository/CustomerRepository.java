package hu.iqjb.spring.repository;

import java.util.List;

public interface CustomerRepository {
    Customer save(Customer account);

    List<Customer> findAll();
}
