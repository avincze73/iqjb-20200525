package hu.iqjb;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class EmployeeService {
    private EmployeeRepository employeeRepository;

    public Employee add(Employee employee) {
        return employeeRepository.add(employee);
    }

    public List<Employee> getAll() {
        return employeeRepository.getAll();
    }

}
