package hu.iqjb.ioc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
public class EmployeeService {

    @Autowired
    private MessageSource messageSource;


    public String toEnglish() {
        return messageSource.getMessage("user.welcome", null,
                "Default Greeting", Locale.ENGLISH);
    }

    public String toHungarian() {
        return messageSource.getMessage("user.welcome", null,
                "Default Greeting", Locale.forLanguageTag("hu"));
    }

}
