package hu.iqjb.testing.department.repository;

import hu.iqjb.testing.department.entity.Department;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
//@SpringBootTest
//@RunWith(SpringRunner.class)
@ActiveProfiles("test")
//@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class DepartmentRepositoryTest {

    //@DataJpaTest provides some standard setup needed for testing the persistence layer:



    private static final Logger logger = Logger.getLogger(DepartmentRepositoryTest.class.getName());

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private DepartmentRepository departmentRepository;

    @BeforeEach
    public void setUp() {
        entityManager.clear();
        entityManager.flush();
        Department it = new Department("IT", "leader1");
        entityManager.persist(it);
        entityManager.flush();
    }

    @Test
    public void whenFindByName_thenReturnDepartment() {
        Optional<Department> departmentList = departmentRepository.findByName("IT");
        assertThat(departmentList.get().getName()).isEqualTo("IT");
    }


}
