package hu.iqjb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void add(Employee employee){
        employeeRepository.save(employee);
        if (employee.getId() == 1){
            throw new RuntimeException();
        }
        employee.setId(employee.getId()+10);
        employeeRepository.save(employee);
    }

    @Transactional
    public List<Employee> getAll(){
        return employeeRepository.findAll();
    }
}
