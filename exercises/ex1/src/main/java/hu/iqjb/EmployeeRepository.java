package hu.iqjb;

import java.util.List;

public interface EmployeeRepository {

    Employee add(Employee employee);
    List<Employee> getAll();
}
