package hu.iqjb.testing.department.repository;

import hu.iqjb.testing.department.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Integer> {


    Optional<Department> findByName(String name);

}
