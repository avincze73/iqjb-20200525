package hu.iqjb;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Repository
public class InMemoryEmployeeRepository implements EmployeeRepository {

    private List<Employee> database;

//    public InMemoryEmployeeRepository() {
//        database = new ArrayList<>();
//    }

    public InMemoryEmployeeRepository(Employee employee1,
                                      Employee employee2,
                                      Employee employee3,
                                      Employee employee4,
                                      Employee employee5
    ) {
        database = new ArrayList<>();
        database.add(employee1);
        database.add(employee2);
        database.add(employee3);
        database.add(employee4);
        database.add(employee5);
    }

    @Override
    public Employee add(Employee employee) {
        database.add(employee);
        return employee;
    }

    @Override
    public List<Employee> getAll() {
        return new ArrayList<>(database);
    }
}
