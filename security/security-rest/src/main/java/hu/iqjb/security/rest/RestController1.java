package hu.iqjb.security.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Map;

@RestController
@Slf4j
public class RestController1 {


    @Autowired
    private SecureService secureService;


    @GetMapping("/api1/greeting")
    @ResponseStatus(value = HttpStatus.OK)
    public String greeting(){
        return "Hello from unsecure rest service";
    }

    @GetMapping("/api1/greeting3/{id}")
    public String greeting(@PathVariable(name = "id") String id){
        if ("11".equals(id)){
            throw new MyResourceNotFoundException();
        } else if ("22".equals(id)){
            throw new ResponseStatusException(
                    HttpStatus.FAILED_DEPENDENCY, "Failed dependency", null);
        }
        return "Hello from unsecure rest service 3";
    }


    @GetMapping("/api1/greeting2")
    public String greeting2(){
        return secureService.greeting();
    }


    @GetMapping("/api1/language")
    public String greeting3(@RequestHeader("accept-language") String language){
        return language;
    }

    @GetMapping("/api1/language2")
    public String greeting4(@RequestHeader(name = "accept-language", defaultValue = "hu") String language){
        return language;
    }

    @GetMapping("/api1/listheaders")
    public ResponseEntity<String> listAllHeaders(
            @RequestHeader Map<String, String> headers) {
        headers.forEach((key, value) -> {
            log.info(String.format("Header '%s' = %s", key, value));
        });

        return new ResponseEntity<String>(
                String.format("Listed %d headers", headers.size()), HttpStatus.OK);
    }

    //An ETag (entity tag) is an HTTP response header returned by
    // an HTTP/1.1 compliant web server used to determine change in content at a
    // given URL.
    @GetMapping("/api1/etag")
    public ResponseEntity<String> etag() {
        return ResponseEntity.ok()
                .eTag(Long.toString(1234L))
                .body("Hello world");
    }

}
