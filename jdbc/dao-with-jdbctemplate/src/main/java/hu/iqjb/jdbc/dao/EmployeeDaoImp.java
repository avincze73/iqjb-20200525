package hu.iqjb.jdbc.dao;

import hu.iqjb.jdbc.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Map;

@Repository
public class EmployeeDaoImp implements EmployeeDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private DataSource dataSource;

    private SimpleJdbcCall jdbcCall;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcCall = new SimpleJdbcCall(this.dataSource).withProcedureName("new_procedure");
    }

    public int getEmployeeCount() {
        String sql = "select count(*) from employee";
        return jdbcTemplate.queryForObject(sql, Integer.class);
    }

    public int insertEmployee(Employee employee) {
        String insertQuery = "insert into employee (EmpId, Name, Age) values (?, ?, ?) ";
        Object[] params = new Object[]{employee.getEmpId(), employee.getName(), employee.getAge()};
        int[] types = new int[]{Types.INTEGER, Types.VARCHAR, Types.INTEGER};
        return jdbcTemplate.update(insertQuery, params, types);
    }

    public Employee getEmployeeById(int empId) {
        String query = "select * from Employee where EmpId = ?";
        // using RowMapper anonymous class, we can create a separate RowMapper
        // for reuse
        Employee employee = jdbcTemplate.queryForObject(query,
                new Object[]{empId},
                new RowMapper<Employee>() {
                    public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
                        Employee employee = new Employee(rs.getInt("EmpId"), rs.getString("Name"), rs.getInt("Age"));
                        return employee;
                    }
                });
        return employee;
    }

    public int deleteEmployeeById(int empId) {
        String delQuery = "delete from employee where EmpId = ?";
        return jdbcTemplate.update(delQuery, new Object[]{empId});
    }

    public void createEmployee() {
        jdbcTemplate.execute("drop table employee");
        jdbcTemplate.execute("create table employee (EmpId integer, Name char(30), Age integer)");
    }

    public Employee getEmployee2(Integer id) {
        SqlParameterSource in = new MapSqlParameterSource().addValue("id", id);
        Map<String, Object> simpleJdbcCallResult = jdbcCall.execute(in);
        System.out.println("" + simpleJdbcCallResult.get("Emp_Name") + simpleJdbcCallResult.get("Emp_Age"));
        Employee employee = new Employee(id, (String) simpleJdbcCallResult.get("Emp_Name"), (Integer) simpleJdbcCallResult.get("Emp_Age"));
        return employee;
    }

}
