package hu.iqjb;

import hu.iqjb.aop.AopConfig;
import hu.iqjb.aop.model.Department;
import hu.iqjb.aop.model.Employee;
import hu.iqjb.aop.service.DepartmentService;
import hu.iqjb.aop.service.EmployeeService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

public class App {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(AopConfig.class);
        EmployeeService employeeService = applicationContext.getBean("employeeService", EmployeeService.class);
        Employee employee = employeeService.getEmployee();
        System.out.println(employee.getEmpName());
        try {
            employeeService.throwException();
        } catch (Exception e) {
            //e.printStackTrace();
        }

        employeeService.addEmployee("András");

//        DepartmentService departmentService = applicationContext.getBean(DepartmentService.class);
//        departmentService.add(new Department());
//        departmentService.getById(1);
//        departmentService.getNameById(1);

    }
}
