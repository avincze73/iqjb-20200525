package hu.iqjb.ex5;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Department {
    private Integer id;
    @NotNull
    private String name;
    @NotNull
    private String headOfDepartment;
    @NotNull
    private String address;
}
