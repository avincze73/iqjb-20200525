package hu.iqjb.aop;


import hu.iqjb.aop.model.Employee;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan(basePackages = "hu.iqjb.aop")
@EnableAspectJAutoProxy
public class AopConfig {

    @Bean
    public Employee employee() {
        Employee emp = new Employee();
        emp.setEmpName("Attila");
        return emp;
    }
}
