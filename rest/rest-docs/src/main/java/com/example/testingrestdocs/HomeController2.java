package com.example.testingrestdocs;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@Controller
public class HomeController2 {

	@GetMapping("/restdoc")
	public String restDocs(){
		return "restdocs";
	}
}
