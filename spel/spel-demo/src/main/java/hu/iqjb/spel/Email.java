package hu.iqjb.spel;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class Email {
    @Value("spam@something.com")
    String emailAddress;


}
