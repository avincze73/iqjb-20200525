package hu.iqjb.mongo;

import com.mongodb.client.MongoClients;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

import java.util.logging.Logger;

import static org.springframework.data.mongodb.core.query.Criteria.where;

/**
 * Hello world!
 */
public class App1 {

    private static final Logger logger = Logger.getLogger(App1.class.getName());


    public static void main(String[] args) {
        MongoOperations mongoOps = new MongoTemplate(MongoClients.create(), "spring-demo-1");
        mongoOps.insert(new Person("Joe", 34));

        logger.info(mongoOps.findOne(new Query(where("name").is("Joe")), Person.class).toString());

        mongoOps.dropCollection("person");
    }
}
