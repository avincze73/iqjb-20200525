package hu.iqjb.spel;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Getter
@Setter
public class Customer {

    private Map<String, String> map;
    private List<String> list;
    public Customer() {
        map = new HashMap<>();
        map.put("emp1", "employee-1 from map");
        map.put("emp2", "employee-2 from map");
        list = new ArrayList<>();
        list.add("employee-1 from list");
        list.add("employee-2 from list");
    }


}
