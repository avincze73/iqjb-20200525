package hu.iqjb.aop.aspect;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.Configuration;

import java.util.logging.Logger;

@Aspect
@Configuration
public class LogExecutionBeforeAspect {

    private static final Logger logger = Logger.getLogger(LogExecutionBeforeAspect.class.getName());


    @Before("@annotation(hu.iqjb.aop.aspect.LogExecutionBefore)")
    public void logExecutionTimeOfMethodCall(JoinPoint joinPoint) throws Throwable {
        logger.info("logBefore() is running " + joinPoint.getSignature().getName());
    }

}
