package hu.iqjb.jpa;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(MyConfiguration.class);

        StudentDaoJpaImpl dao = (StudentDaoJpaImpl) applicationContext.getBean("studentDao");

        Student student = new Student();
        student.setFirstName("Joe");
        student.setLastName("Smith");

        dao.save(student);
    }

}