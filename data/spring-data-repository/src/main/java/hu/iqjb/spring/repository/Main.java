package hu.iqjb.spring.repository;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import java.util.List;


public class Main {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        //ApplicationContext applicationContext = new AnnotationConfigApplicationContext(InfrastructureConfig.class);
        //CustomerRepository repo = applicationContext.getBean(CustomerRepository.class);

        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        CustomerRepositorySpring repo = applicationContext.getBean(CustomerRepositorySpring.class);


        Customer c = new Customer();
        c.setFirstname("attila");
        c.setLastname("vincze");
        c.setEmailAddress("Fót");
        repo.save(c);

        List<Customer> result = repo.findAll();
        result.stream().forEach(System.out::println);

        Customer e = repo.findByFirstname("attila");
        System.out.println(e);


        Pageable pageable = new PageRequest(2, 10, Direction.ASC, "lastname", "firstname");
        Page<Customer> result2 = repo.findByEmailAddress("Fót", pageable);


        Sort sort = new Sort(Direction.DESC, "firstname");
        List<Customer> result3 = repo.findByAddress("Budapest", sort);


    }

}
