package hu.iqjb;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "hu.iqjb")
public class EmployeeConfiguration {
    @Bean
    public Employee employee1(){
        Employee employee = new Employee();
        employee.setId(1);
        employee.setTitle("title1");
        employee.setFirstName("firstName1");
        employee.setLastName("lastName1");
        employee.setLoginName("loginName1");
        employee.setPassword("password1");
        employee.setSalary(1000);
        return employee;
    }

    @Bean
    public Employee employee2(){
        Employee employee = new Employee();
        employee.setId(2);
        employee.setTitle("title2");
        employee.setFirstName("firstName2");
        employee.setLastName("lastName2");
        employee.setLoginName("loginName2");
        employee.setPassword("password2");
        employee.setSalary(2000);
        return employee;
    }


    @Bean
    public Employee employee3(){
        Employee employee = new Employee();
        employee.setId(3);
        employee.setTitle("title3");
        employee.setFirstName("firstName3");
        employee.setLastName("lastName3");
        employee.setLoginName("loginName3");
        employee.setPassword("password3");
        employee.setSalary(3000);
        return employee;
    }


    @Bean
    public Employee employee4(){
        Employee employee = new Employee();
        employee.setId(4);
        employee.setTitle("title4");
        employee.setFirstName("firstName4");
        employee.setLastName("lastName4");
        employee.setLoginName("loginName4");
        employee.setPassword("password4");
        employee.setSalary(4000);
        return employee;
    }


    @Bean
    public Employee employee5(){
        Employee employee = new Employee();
        employee.setId(5);
        employee.setTitle("title5");
        employee.setFirstName("firstName5");
        employee.setLastName("lastName5");
        employee.setLoginName("loginName5");
        employee.setPassword("password5");
        employee.setSalary(5000);
        return employee;
    }

//    @Bean
//    public InMemoryEmployeeRepository employeeRepository(){
//        InMemoryEmployeeRepository inMemoryEmployeeRepository =
//                new InMemoryEmployeeRepository();
//        inMemoryEmployeeRepository.getDatabase().add(employee1());
//        inMemoryEmployeeRepository.getDatabase().add(employee2());
//        inMemoryEmployeeRepository.getDatabase().add(employee3());
//        inMemoryEmployeeRepository.getDatabase().add(employee4());
//        inMemoryEmployeeRepository.getDatabase().add(employee5());
//        return inMemoryEmployeeRepository;
//    }
//
//    @Bean
//    public EmployeeService employeeService(){
//        EmployeeService employeeService = new EmployeeService();
//        employeeService.setEmployeeRepository(employeeRepository());
//        return employeeService;
//    }
//
    
}
