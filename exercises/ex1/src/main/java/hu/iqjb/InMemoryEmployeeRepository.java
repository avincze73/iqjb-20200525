package hu.iqjb;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
public class InMemoryEmployeeRepository implements EmployeeRepository {

    private List<Employee> database;

    public InMemoryEmployeeRepository() {
        database = new ArrayList<>();
    }

    @Override
    public Employee add(Employee employee) {
        database.add(employee);
        return employee;
    }

    @Override
    public List<Employee> getAll() {
        return new ArrayList<>(database);
    }
}
