package hu.iqjb.mybatis;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
public interface ArticleRepository {
    @Select("SELECT * FROM ARTICLES WHERE id = #{id}")
    Article findById(@Param("id") Long id);

    @Insert("INSERT into articles(id,title,author) VALUES(#{id}, #{title}, #{author})")
    void add(Article article);

    @Select("select * from articles")
    List<Article> findAll();

    @Delete("DELETE FROM articles WHERE id = #{id}")
    int deleteById(long id);

    @Delete("DELETE FROM articles")
    int clean();

    @Update("Update articles set title=#{title}, " +
            " author=#{author} where id=#{id}")
    int update(Article article);
}
