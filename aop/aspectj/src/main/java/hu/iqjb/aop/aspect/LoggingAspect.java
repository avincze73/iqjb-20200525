package hu.iqjb.aop.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;


@Component
@Aspect
public class LoggingAspect {

    @Pointcut("execution(* hu.iqjb.aop.service.EmployeeService.getEmployee(..))") // the pointcut expression
    public void serviceMethod() { // the pointcut signature
    }


    @Pointcut("execution(* hu.iqjb.aop.model.Employee.getEmpName(..))") // the pointcut expression
    public void serviceMethod2() { // the pointcut signature
    }

//    @Pointcut("execution(* hu.iqjb.aop.service.EmployeeService.addEmployee(..))") // the pointcut expression
//    public void serviceMethod3() { // the pointcut signature
//    }


    //@Around("serviceMethod3()")
    @Around("execution(* hu.iqjb.aop.service.EmployeeService.addEmployee(..))")
    //@Pointcut("execution(* hu.iqjb.aop.service.EmployeeService.addEmployee(..))") // the pointcut expression
    public void logAround(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("logAround() is running!");
        System.out.println("hijacked method : " + joinPoint.getSignature().getName());
        System.out.println("hijacked arguments : " + Arrays.toString(joinPoint.getArgs()));

        System.out.println("Around before is running!");
        joinPoint.proceed(); //continue on the intercepted method
        System.out.println("Around after is running!");

        System.out.println("******");
    }

    @Before("serviceMethod()")
    public void logBefore(JoinPoint joinPoint) {
        System.out.println("logBefore() is running " + joinPoint.getSignature().getName());
    }

    @After("serviceMethod()")
    public void logAfter(JoinPoint joinPoint) {
        System.out.println("logAfter() is running " + joinPoint.getSignature().getName());
    }

    @AfterReturning(pointcut = "serviceMethod2()", returning = "result")
    public void logAfterReturning(JoinPoint joinPoint, Object result) {
        System.out.println("logAfterReturning() is running!");
        System.out.println("hijacked : " + joinPoint.getSignature());
        System.out.println("Method returned value is : " + result);
        System.out.println("******");
    }

    @AfterThrowing(
            pointcut = "execution(* hu.iqjb.aop.service.EmployeeService.throwException(..))",
            throwing = "error")
   public void logAfterThrowing(JoinPoint joinPoint, Throwable error) {
        System.out.println("logAfterThrowing() is running!");
        System.out.println("hijacked : " + joinPoint.getSignature().getName());
        System.out.println("Exception : " + error);
        System.out.println("******");
    }
}
