package hu.iqjb.security.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class RestController2 {

    @GetMapping("/api2/greeting")
    public String greeting(){
        return "Hello from secure rest service";
    }
}
