package hu.iqjb.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;


@Service
public class ServiceFacade {

    @Autowired
    private BookDao bookDao;

    @Autowired
    private DepartmentDao departmentDao;

    public void setBookDao(BookDao bookDao) {
        this.bookDao = bookDao;
    }

    public void setDepartmentDao(DepartmentDao departmentDao) {
        this.departmentDao = departmentDao;
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = MyException3.class)
    public void save(Book book, Department department) throws Exception {
        bookDao.save(book);
        //TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        if ("book2".equals(book.getName())) {
            throw new MyException3();
        }
        departmentDao.save(department);
        if(TransactionAspectSupport.currentTransactionStatus().isRollbackOnly()) {
            departmentDao.save(department);
        }
    }

}
