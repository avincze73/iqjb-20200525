package hu.iqjb.mongo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    public void save(){
        Person p = new Person("Joe", 34);
        personRepository.save(p);
         p = new Person("Jim", 12);
        personRepository.save(p);
    }

    public List<Person> getAll(){
        List<Person> personList = personRepository.findAll(Sort.by("age"));
        personList.forEach(person -> System.out.println(person.getName()));
        return personList;
    }
}
