package hu.iqjb.ex5;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.support.SessionStatus;

import javax.validation.Valid;

@Controller
@Slf4j
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private SessionInfo sessionInfo;

    @GetMapping("/department")
    public String departmentGet(Model model){
        sessionInfo.setNumber(sessionInfo.getNumber()+1);
        log.info(sessionInfo.getNumber()+"");
        model.addAttribute("entity", new Department());
        return "department";
    }

    @GetMapping("/departments")
    public String departmentsGet(Model model){
        sessionInfo.setNumber(sessionInfo.getNumber()+1);
        log.info(sessionInfo.getNumber()+"");
        model.addAttribute("entityList", departmentService.getAll());
        return "departments";
    }


    @PostMapping("/department")
    public String entitySave(@ModelAttribute("entity") @Valid Department entity,
                             BindingResult errors, SessionStatus status){
        sessionInfo.setNumber(sessionInfo.getNumber()+1);
        log.info(sessionInfo.getNumber()+"");
        if (errors.hasErrors()){
            return "/department";
        }
        log.info(departmentService.getAll().size() + " ");
        entity.setId(departmentService.getAll().size() + 1);
        departmentService.getAll().add(entity);
        status.setComplete();
        return "redirect:/departments";
    }

}
