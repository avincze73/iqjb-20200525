package hu.iqjb.ioc;

public class EmployeeWithInheritance {

	private String employeeName;
	private String employeeTitle;

	public String getEmployeeTitle() {
		return employeeTitle;
	}

	public void setEmployeeTitle(String employeeTitle) {
		this.employeeTitle = employeeTitle;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	@Override
	public String toString() {

		return "Employee Name: " + this.employeeName + " " + this.employeeTitle;
	}
}
