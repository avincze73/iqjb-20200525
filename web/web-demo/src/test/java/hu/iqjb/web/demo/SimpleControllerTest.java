package hu.iqjb.web.demo;

import hu.iqjb.web.demo.controller.MainController;
import hu.iqjb.web.demo.controller.SimpleController;
import hu.iqjb.web.demo.properties.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.BDDMockito.given;

@WebMvcTest(SimpleController.class)
//@ConfigurationPropertiesScan("hu.iqjb.web.demo.properties")
@Slf4j
public class SimpleControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IqjbProperties iqjbProperties;

    @Test
    void shouldLoadContext() {
    }

    @Test
    void shouldReturnGreeting() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/simple"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Hello")));

    }

    @Test
    void shouldReturnHostName() throws Exception {
        BDDMockito.given(iqjbProperties.getHostname()).willReturn("index.hu");
        this.mockMvc.perform(MockMvcRequestBuilders.get("/simple"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("index.hu")));

    }
}
