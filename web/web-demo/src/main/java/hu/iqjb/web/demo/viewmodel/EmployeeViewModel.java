package hu.iqjb.web.demo.viewmodel;

import hu.iqjb.web.demo.controller.Employee;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.annotation.SessionScope;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@SessionScope
@Component
public class EmployeeViewModel implements Serializable {

    private List<Employee> employeeList;

    public EmployeeViewModel() {
        this.employeeList = new ArrayList<>();
    }
}
