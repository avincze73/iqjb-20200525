package hu.iqjb;

import lombok.*;
import org.springframework.stereotype.Component;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class Employee {
    private Integer id;
    private String title;
    private String firstName;
    private String lastName;
    private String loginName;
    private String password;
    private Integer salary;
}
