# Testing in Spring Boot

## Integration Tests vs. Unit Tests

Unit test
- Covers a single unit, where a unit commonly is a single class, but can also be a cluster of cohesive classes that is tested in combination.

Integration test
- It tests the interaction between two or more clusters of cohesive classes.
- Covers multiple layers.

@SpringBootTest
- Create an application context containing all objects.
- @WebMvcTest or @DataJpaTest 

Alternatives
- @JsonTest - for testing the JSON marshalling and unmarshalling
- @DataJpaTest - for testing the repository layer
- @RestClientTests - for testing REST clients

## Unit tests naming conventions

MethodName_StateUnderTest_ExpectedBehavior
- isAdult_AgeLessThan18_False
- withdrawMoney_InvalidAccount_ExceptionThrown
- admitStudent_MissingMandatoryFields_FailToAdmit

MethodName_ExpectedBehavior_StateUnderTest
- isAdult_False_AgeLessThan18
- withdrawMoney_ThrowsException_IfAccountIsInvalid
- admitStudent_FailToAdmit_IfMandatoryFieldsAreMissing

test[Feature being tested]
- testIsNotAnAdultIfAgeLessThan18
- testFailToWithdrawMoneyIfAccountIsInvalid
- testStudentIsNotAdmittedIfMandatoryFieldsAreMissing

Feature to be tested
- IsNotAnAdultIfAgeLessThan18
- FailToWithdrawMoneyIfAccountIsInvalid
- StudentIsNotAdmittedIfMandatoryFieldsAreMissing

Should_ExpectedBehavior_When_StateUnderTest
- Should_ThrowException_When_AgeLessThan18
- Should_FailToWithdrawMoney_ForInvalidAccount
- Should_FailToAdmit_IfMandatoryFieldsAreMissing

When_StateUnderTest_Expect_ExpectedBehavior
- When_AgeLessThan18_Expect_isAdultAsFalse
- When_InvalidAccount_Expect_WithdrawMoneyToFail
- When_MandatoryFieldsAreMissing_Expect_StudentAdmissionToFail

Given_Preconditions_When_StateUnderTest_Then_ExpectedBehavior
- Given_UserIsAuthenticated_When_InvalidAccountNumberIsUsedToWithdrawMoney_Then_TransactionsWillFail


## @RunWith vs @SpringBootTest
@RunWith(SpringRunner.class)
- You need this annotation to just enable spring boot features like @Autowire, @MockBean etc.. during junit testing

@SpringBootTest
- This annotation is used to load complete application context for end to end integration testing