package hu.iqjb.aop.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;


@Component
@Aspect
public class LoggingAspect2 {

    //pointcut designator (PCD)
    @Pointcut("execution(public String hu.iqjb.aop.service.DepartmentService.getNameById(Integer))") // the pointcut expression
    public void fullSignature1() { // the pointcut signature
    }

    @Pointcut("execution(* hu.iqjb.aop.service.DepartmentService.*(..))") // the pointcut expression
    public void fullSignature2() { // the pointcut signature
    }

    @Pointcut("within(hu.iqjb.aop.service.DepartmentService)") // the pointcut expression
    public void fullSignature3() { // the pointcut signature
    }

    @Pointcut("within(hu.iqjb.aop.service..*)") // the pointcut expression
    public void fullSignature4() { // the pointcut signature
    }

    @Pointcut("within(hu.iqjb.aop.service..*)") // the pointcut expression
    public void fullSignature5() { // the pointcut signature
    }

    @Pointcut("execution(* *..get*(Integer))") // the pointcut expression
    public void fullSignature6() { // the pointcut signature
    }

    @Pointcut("@args(hu.iqjb.aop.aspect.LogExecutionBefore)") // the pointcut expression
    public void fullSignature7() { // the pointcut signature
    }

    @Pointcut("@annotation(hu.iqjb.aop.aspect.LogExecutionBefore)")
    public void loggableMethods() {}


    @Before("fullSignature6()")
    //@Before("fullSignature2()")
    //@Before("fullSignature3()")
    //@Before("fullSignature6()")
    public void logBefore(JoinPoint joinPoint) {
        System.out.println("fullSignature6() is running " + joinPoint.getSignature().getName());
    }

    @Before("fullSignature6() || loggableMethods()")
    public void logBefore2(JoinPoint joinPoint) {
        System.out.println("fullSignature2() is running " + joinPoint.getSignature().getName());
    }

}
