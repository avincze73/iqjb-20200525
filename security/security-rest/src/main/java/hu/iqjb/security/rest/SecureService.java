package hu.iqjb.security.rest;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import javax.annotation.security.RolesAllowed;

@Service
public class SecureService {

    @Secured({ "ROLE_ADMIN", "ROLE_USER" })
    @RolesAllowed({ "ROLE_ADMIN", "ROLE_USER" })
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public String greeting(){
        return "greeting from secure service";
    }
}
