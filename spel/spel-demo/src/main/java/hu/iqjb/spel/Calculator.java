package hu.iqjb.spel;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class Calculator {

    /* Relational operators */
    @Value("#{1 == 1}")// true
    private boolean equal;
    @Value("#{1 != 1}") // false
    private boolean notEqual;
    @Value("#{1 < 1}") // false
    private boolean lessThan;
    @Value("#{1 <= 1}") // true
    private boolean lessThanOrEqual;
    @Value("#{1 > 1}")// false
    private boolean greaterThan;
    @Value("#{1 >= 1}")// true
    private boolean greaterThanOrEqual;

    /* Logical operators , numberBean.no == 999 */
    @Value("#{number.no == 999 and number.no < 900}") // false
    private boolean and;
    @Value("#{number.no == 999 or number.no < 900}")// true
    private boolean or;
    @Value("#{!(number.no == 999)}")// false
    private boolean not;

    /* Mathematical operators */
    @Value("#{1 + 1}")// 2.0
    private double addition;
    @Value("#{1 - 1}")// 0.0
    private double subtraction;
    @Value("#{1 * 1}")// 1.0
    private double multiplication;
    @Value("#{10 / 2}") // 5.0
    private double division;
    @Value("#{10 % 10}")// 0.0
    private double modulus;


}
