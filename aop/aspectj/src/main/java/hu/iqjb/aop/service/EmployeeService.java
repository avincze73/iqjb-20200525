package hu.iqjb.aop.service;


import hu.iqjb.aop.model.Employee;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Getter
public class EmployeeService {

    @Autowired
    private Employee employee;

    public void throwException() throws Exception {
        System.out.println("throwException() is called");
        throw new Exception("Error from employeeservice");
    }

    public void addEmployee(String name) {
        System.out.println("addEmployee is called: " + name);
    }
}
