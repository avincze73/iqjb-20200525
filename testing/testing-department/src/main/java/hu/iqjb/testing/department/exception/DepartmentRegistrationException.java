package hu.iqjb.testing.department.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class DepartmentRegistrationException extends RuntimeException {
    public DepartmentRegistrationException(String message) {
        super(message);
    }

    public DepartmentRegistrationException(String message, Throwable cause) {
        super(message, cause);
    }
}