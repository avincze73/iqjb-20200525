package hu.iqjb.main.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.client.RestTemplate;

@Service
public class MainService {

    @Autowired
    private RestTemplate restTemplate;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void start(){

        restTemplate.getInterceptors().add(new BasicAuthenticationInterceptor("user", "password"));
        ResponseEntity<Void> entity = this.restTemplate.getForEntity(
                "http://localhost:8081/save", Void.class);

        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();


        ResponseEntity<Void> entity2 = this.restTemplate.getForEntity(
                "http://localhost:8082/save", Void.class);
    }
}
