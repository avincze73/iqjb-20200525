package hu.iqjb.ioc;

import org.springframework.beans.factory.InitializingBean;

public class Atm /*implements InitializingBean*/ {
	private Printer printer;

	public Printer getPrinter() {
		return printer;
	}

	public void setPrinter(Printer printer) {
		this.printer = printer;
	}

	public void printBalance(String accountNumber) {
		getPrinter().printBalance(accountNumber);
	}

	//@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("init from Atm");
	}
}
