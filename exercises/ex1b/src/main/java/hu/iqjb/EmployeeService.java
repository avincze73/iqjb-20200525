package hu.iqjb;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.ws.ServiceMode;
import java.util.List;

@Getter
@Setter
@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public Employee add(Employee employee) {
        return employeeRepository.add(employee);
    }

    public List<Employee> getAll() {
        return employeeRepository.getAll();
    }

}
