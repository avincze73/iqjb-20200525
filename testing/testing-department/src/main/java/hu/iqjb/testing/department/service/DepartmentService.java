package hu.iqjb.testing.department.service;

import hu.iqjb.testing.department.entity.Department;
import hu.iqjb.testing.department.exception.DepartmentRegistrationException;
import hu.iqjb.testing.department.repository.DepartmentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    public Department save(Department department) {
        Optional<Department> departmentOptional = departmentRepository.findByName(department.getName());
        if(departmentOptional.isPresent()) {
            throw new DepartmentRegistrationException("User with name "+ departmentOptional.get().getName()+" already exists");
        }

        return departmentRepository.save(department);
    }

    public Department update(Department department) {
        return departmentRepository.save(department);
    }

    public List<Department> getAll() {
        return departmentRepository.findAll();
    }

    public Optional<Department> getById(Integer id) {
        return departmentRepository.findById(id);
    }

    public void deleteById(Integer id){
        departmentRepository.deleteById(id);
    }

}
