package hu.iqjb.spel;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class Address {

    @Value("12345")
    private Long id;
    @Value("Kalvin")
    private String streetName;
    @Value("Hungary")
    private String country;

    public String getFullAddress() {
        return streetName + " " + country;
    }
}
