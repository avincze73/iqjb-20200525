package hu.iqjb.ex5;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeService implements Serializable {


    @Autowired
    private Database database;

    @LogExecutionTime
    public void add(Employee employee){
        database.getEmployeeList().add(employee);
    }

    @LogExecutionTime
    public List<Employee> getAll(){
        return database.getEmployeeList();
    }

}
