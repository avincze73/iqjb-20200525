package hu.iqjb.ex5;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import java.io.Serializable;

@Getter
@Setter
@SessionScope
@Component
public class SessionInfo implements Serializable {
    private Integer number = 0;
}
