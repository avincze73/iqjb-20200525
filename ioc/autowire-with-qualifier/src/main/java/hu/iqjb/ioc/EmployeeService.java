package hu.iqjb.ioc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class EmployeeService {


    @Autowired
    @Qualifier("employeeA")
    private Employee employee;


    public Employee getEmployee() {
        return employee;
    }

    @PostConstruct
    public void init() {

    }

}
