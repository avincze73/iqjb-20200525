package hu.iqjb.ioc;

public class UserServiceImpl2 implements UserService {
    private UserDao userDao;

    public UserServiceImpl2(UserDao userDao) {
        super();
        this.userDao = userDao;
    }

    public UserDao getUserDao() {
        return userDao;
    }

    public Long getId() {
        return System.currentTimeMillis();
    }

}
