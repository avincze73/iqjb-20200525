package hu.iqjb.web.demo.controller;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class Employee {

    private Integer id;
    private String title;
    @NotEmpty(message = "töltsd ki")
    private String firstName;
    private String lastName;
    private String loginName;
    @Size(min = 6, max = 20)
    @NotEmpty
    private String password;
    @Min(80000)
    @NotNull
    private Integer salary;
}
