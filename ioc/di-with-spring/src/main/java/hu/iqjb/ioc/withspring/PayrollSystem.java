package hu.iqjb.ioc.withspring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class PayrollSystem {

    public static void main(String[] args) {

        ApplicationContext context =
                new ClassPathXmlApplicationContext("beans.xml");

        ApplicationContext context2 =
                new ClassPathXmlApplicationContext("beans.xml");

        EmployeeService employeeService =
                (EmployeeService) context.getBean("employeeServiceBean");

        EmployeeService employeeService2 =
                (EmployeeService) context.getBean("employeeServiceBean");

        System.out.println(employeeService.generateEployeeID());
        System.out.println(employeeService.generateEployeeID());


    }

}
