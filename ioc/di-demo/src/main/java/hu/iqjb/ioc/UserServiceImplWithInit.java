package hu.iqjb.ioc;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class UserServiceImplWithInit implements UserService/*, InitializingBean, DisposableBean */{
	private UserDao userDao;

	public UserServiceImplWithInit() {
		super();
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public Long getId() {
		return System.currentTimeMillis();
	}

	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("Employee afterPropertiesSet... ");

	}

	public void destroy() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("Employee destroy... ");

	}

}
