package hu.iqjb.security.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.filter.ShallowEtagHeaderFilter;

@SpringBootApplication
public class SecurityRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecurityRestApplication.class, args);
	}


}
