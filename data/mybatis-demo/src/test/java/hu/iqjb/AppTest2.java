package hu.iqjb;

import hu.iqjb.mybatis.Article;
import hu.iqjb.mybatis.ArticleRepository;
import hu.iqjb.mybatis.MyService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;


/**
 * Unit test for simple App.
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = MyTestConfiguration.class)
public class AppTest2
{

    @Autowired
    private MyService myService;

    @Autowired
    private ArticleRepository articleRepository;


    @Test
    //Smoke test
    public void shouldAnswerWithTrue()
    {
        assertThat(myService).isNotNull();
        assertThat(articleRepository).isNotNull();
        assertThat( true );
    }

    @BeforeEach
    public void setUp(){
        Article article = new Article();
        article.setId(1L);
        article.setTitle("title1");
        article.setAuthor("author1");;
        articleRepository.add(article);
    }

    @AfterEach
    public void cleanUp(){
        articleRepository.clean();
    }

    @Test
    public void shouldReturnArticleById() {
        Article article = articleRepository.findById(1L);

        assertThat(article).isNotNull();
        assertThat(article.getId()).isEqualTo(1L);
        assertThat(article.getAuthor()).isEqualTo("author1");
        assertThat(article.getTitle()).isEqualTo("title1");
    }

    @Test
    public void shouldReturnTwoArticlesWhenFindAll() {
        Article article = new Article();
        article.setId(2L);
        article.setTitle("title2");
        article.setAuthor("author2");;
        articleRepository.add(article);
        List<Article> articleList = articleRepository.findAll();

        assertThat(articleList).isNotNull();
        assertThat(articleList).hasSize(2);
    }

    @Test
    public void shouldUpdateSucessfully() {
        Article article = new Article();
        article.setId(1L);
        article.setTitle("title1b");
        article.setAuthor("author1b");;
        articleRepository.update(article);

        article = articleRepository.findById(1L);

        assertThat(article).isNotNull();
        assertThat(article.getAuthor()).isEqualTo("author1b");
        assertThat(article.getTitle()).isEqualTo("title1b");
    }

    @Test
    public void shouldDeleteSucessfully() {


        articleRepository.deleteById(1L);

        Article article = articleRepository.findById(1L);

        assertThat(article).as("it is failed").isNull();
    }


    @Test
    void shouldCatchException() {

        assertThatThrownBy(() -> divide(1, 0))
                .isInstanceOf(ArithmeticException.class)
                .hasMessageContaining("zero")
                .hasMessage("/ by zero");

        assertThatThrownBy(() -> {
            List<String> list = Arrays.asList("one", "two");
            list.get(2);
        })
                .isInstanceOf(IndexOutOfBoundsException.class)
                .hasMessageContaining("2");

    }

    int divide(int input, int divide) {
        return input / divide;
    }
}
