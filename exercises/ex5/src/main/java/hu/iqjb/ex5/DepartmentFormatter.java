package hu.iqjb.ex5;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Locale;

//@Component
public class DepartmentFormatter implements Formatter<Department> {

    @Autowired
    private DepartmentService departmentService;


    @Override
    public Department parse(String s, Locale locale) throws ParseException {
        return departmentService.getByName(s);
    }

    @Override
    public String print(Department department, Locale locale) {
        return (department != null ? department.getName() : "");
    }
}
