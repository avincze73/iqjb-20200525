package hu.iqjb.jpa;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class BookDaoJpaImpl implements BookDao {

    @PersistenceContext
    private EntityManager entityManager;


    @Transactional(propagation = Propagation.REQUIRED)
    public void save(Book book) {
        entityManager.persist(book);
        entityManager
                .createQuery("select b from Book b where b.id =1", Book.class)
                .getSingleResult();
    }
}
