package hu.iqjb.aop.service;

import hu.iqjb.aop.aspect.LogExecutionBefore;
import hu.iqjb.aop.aspect.LogExecutionTime;
import hu.iqjb.aop.model.Department;
import org.springframework.stereotype.Service;

@Service
public class DepartmentService {

    @LogExecutionBefore
    public void add(Department department){

    }


    @LogExecutionBefore
    public Department getById(Integer id){
        return new Department(1, "name1");
    }

    @LogExecutionTime
    @LogExecutionBefore
    public String getNameById(Integer id){
        return "";
    }
}
