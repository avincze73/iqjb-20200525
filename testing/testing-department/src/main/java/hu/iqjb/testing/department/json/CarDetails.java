package hu.iqjb.testing.department.json;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CarDetails {
    private String manufacturer;
    private String type;
    private String color;
}
