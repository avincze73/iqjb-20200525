package hu.iqjb.ioc;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class EmployeeWithCollections {
	private List<String> lists;
	private Set<String> sets;
	private Map<String, String> maps;

	public void setLists(List<String> lists) {
		this.lists = lists;
	}

	public void setSets(Set<String> sets) {
		this.sets = sets;
	}

	public void setMaps(Map<String, String> maps) {
		this.maps = maps;
	}

}
